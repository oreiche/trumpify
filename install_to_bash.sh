#!/bin/bash

cp trumpify.db ~/.trumpify.db
mkdir -p ~/.local/bin
cp trumpify.sh ~/.local/bin/

if [ ! "$(grep trumpify ~/.bashrc)" ]; then
  echo -e "\n~/.local/bin/trumpify.sh" >> ~/.bashrc
fi
